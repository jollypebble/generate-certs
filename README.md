# Jolly Pebble Docker Certificate Generator

This image generates a self-signed SSL/TLS certificate.

Set the certificate server name with the SERVER environment variable via a command line parameter.

The certificate subject organization is specified using the commandline option to set the SUBJECT environment variable.

An example of generating a certificate for server 'server.example.com' using the standard CA organization (assuming you tag the image as jollypebble/genearate-cert):

- ```docker run --rm -v $(pwd):/certificates -e 'SERVER=server.example.com' jollypebble/generate-certs```

Or if you want to set, for example an organization:

- ```docker run --rm -v $(pwd):/certificates -e 'SERVER=server.example.com' -e '/C=US/ST=New York/L=New York/O=Jolly Pebble Technologies/OU=Development/CN=server.example.com' jollypebble/generate-certs```

for the specified server will generate:

  * cacert.pem
  * server.key
  * server.pem
